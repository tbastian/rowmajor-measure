TARGET=main.bin
CFLAGS=-O2 -g -std=c11
CC=gcc
CLIBS=-lpapi

all: $(TARGET)

%.bin: %.o
	$(CC) $(CFLAGS) $(CLIBS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
