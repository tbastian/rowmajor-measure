# Rowmajor vs. colmajor experiments

This repository implements a simple sum of the elements of a (large) array,
either iterating row-major, or column-major, ie.
```
for(row)
    for(col)
        …
```
or
```
for(col)
    for(row)
        …
```

and measures the performance of both using PAPI counters.

## Running

```bash
echo 0 | sudo tee /proc/sys/kernel/perf_event_paranoid  # allow papi to read HW counters
make
export PAPI_EVENTS="PAPI_TOT_CYC,PAPI_L1_DCM,PAPI_L2_DCM"
./main.bin
# Result is in papi_hl_output
```

## Results on Intel(R) Xeon(R) CPU E3-1505M v6

Parameters:
```
#define MATRIX_ROWS 200000
#define MATRIX_COLS 2000
#define NUM_MEASURES 10
```

Results:
* Row-major
    * cycles: 16954479108
    * L1d misses: 500029527
* Col-major
    * cycles: 99027754836
    * L1d misses: 4019390821
* Ratio:
    * x5.84 speedup
    * x8.04 L1d misses
