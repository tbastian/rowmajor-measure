#include <papi.h>
#include <stdio.h>
#include <stdlib.h>

#define MATRIX_ROWS 200000 // A full column must not fit in the CPU cache
#define MATRIX_COLS 2000
#define NUM_MEASURES 10

double rowmajor_sum(double (*matrix)[MATRIX_COLS]) {
    double out = 0;
    for(int row=0; row < MATRIX_ROWS; ++row) {
        for(int col=0; col < MATRIX_COLS; ++col) {
            out += matrix[row][col];
        }
    }
    return out;
}

double colmajor_sum(double (*matrix)[MATRIX_COLS]) {
    double out = 0;
    for(int col=0; col < MATRIX_COLS; ++col) {
        for(int row=0; row < MATRIX_ROWS; ++row) {
            out += matrix[row][col];
        }
    }
    return out;
}

int main(void) {
    srand(0);

    double (*matrix)[MATRIX_COLS] = malloc(sizeof(double) * MATRIX_ROWS * MATRIX_COLS);
    if(matrix == NULL) {
        fprintf(stderr, "Failed to malloc\n");
        exit(1);
    }
    for(int row=0; row < MATRIX_ROWS; ++row) {
        for(int col=0; col < MATRIX_COLS; ++col) {
            matrix[row][col] = rand();
        }
    }

    printf("Rowmajor: ");
    for(int measure=0; measure < NUM_MEASURES; measure++) {
        PAPI_hl_region_begin("rowmajor");
        printf("%lf - ", rowmajor_sum(matrix));
        PAPI_hl_region_end("rowmajor");
        fflush(stdout);
    }
    printf("\n");

    printf("Colmajor: ");
    for(int measure=0; measure < NUM_MEASURES; measure++) {
        PAPI_hl_region_begin("colmajor");
        printf("%lf - ", colmajor_sum(matrix));
        PAPI_hl_region_end("colmajor");
        fflush(stdout);
    }
    printf("\n");

    free(matrix);

    return 0;
}
